<?php
require
str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'public/blog-project/app/config/config.php';
$smarty = new Smarty;


/*
セッションを確認してIDを取得する
DBからユーザー情報をよみこみ
HTMLに出力する
データの書き込みを受け付ける
UPDATE文で起こす
*/

/*********************
バリデーションの実施
ポスト値で読み込みを行う入力値のチェック
*********************/
   $errors = array();//エラーの初期化
   $input_name = filter_input(INPUT_POST,'name');
   $input_email = filter_input(INPUT_POST,'email');
   $input_password = filter_input(INPUT_POST,'password');
   $input_age = filter_input(INPUT_POST,'age');


if($_SERVER["REQUEST_METHOD"] == "POST"){

	//名前
	if(!isset($input_name)||strlen($input_name)<=0){
	$errors['name'] = '名前が入力されていません。';
	}
	elseif(strlen($input_name)>=100){
			$errors['name'] = '名前は100文字以内で入力してください';
		}

	//email
	if(!isset($input_email)||strlen($input_email)<=0){
		$errors['email'] = 'メールが入力されていません';
	}
	//password
	if(!isset($input_password)||strlen($input_password)<=0){
		$errors['password'] = 'メールが入力されていません';
	}
	// 	//age
	// if(!isset($input_age)||strlen($input_age)<=0){
	// 	$errors['age'] = '年齢が入力されていません';
	// }
}


/***************************
編集用データの表示を行います。
****************************/
	//プリペアステートメント

if($input_name && $input_email && $input_password && !isset($errors['name']) && !isset($errors['email']) && !isset($errors['password'])){
    // ここでSQL文を作成します（パラメータはありません）
    $stmt = $mysqli->prepare("UPDATE users SET name=?, email=?, password=?, modified=? WHERE id=?");
    // ここでパラメータに実際の値となる変数を入れます。
    // sssdのところは、それぞれパラメータの型（string, string, string, double）を指定しています。
    $rc = $stmt->bind_param('ssssi', $input_name, $input_email, $input_password, date('Y-m-d H:i:s'), $_SESSION['blog_login']['id']);

    if ( false===$rc ) {
        // again execute() is useless if you can't bind the parameters. Bail out somehow.
        die('bind_param() failed: ' . htmlspecialchars($stmt->error));
    }
    /* プリペアドステートメントを実行します */
    $rc = $stmt->execute();
    if ( false===$rc ) {
        die('execute() failed: ' . htmlspecialchars($stmt->error));
    }
    // $stmt->affected_rowsでクエリ結果を取得しています。これはInsert文などで変更された行の数を返します。
    //printf("%d Row inserted.\n", $stmt->affected_rows);

    /* ステートメントと接続を閉じます */
    $stmt->close();//リソースの開放
    //sessionの上書き
    $_SESSION['blog_login']['name'] = $input_name;
    header( "Location: /blog-project/public/admin/account/edit.php?flash_msg=account updated." ) ;
    exit(1);
}


/****************
 * アカウントの表示
 * SELECT
 *****************/
if($_SESSION['blog_login']['id']){
  // プリペアステートメント
  $sql = "SELECT id, name, email, password FROM users WHERE id=?";
  if ($stmt = $mysqli->prepare($sql)) {
      // 条件値をSQLにバインドする（補足参照）
      $stmt->bind_param("i", $_SESSION['blog_login']['id']);
      // 実行
      $stmt->execute();
      $stmt->bind_result($id, $name, $email, $password);
      $stmt->store_result();

      while ($stmt->fetch()) {

        if($id){
          $smarty->assign("id", $id);
          $smarty->assign("name", $name);
          $smarty->assign("email", $email);
          $smarty->assign("password", $password);
        }
        else{
          header( "Location: /404.php" ) ;
          exit(1);
        }
        $stmt->close();//リソースの開放
      }
  }
}
else{
  header( "Location: /404.php" );
  exit(1);
}

$smarty->display('./admin/account/account.html');

?>