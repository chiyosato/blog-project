<?php
require
str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'public/blog-project/app/config/config.php';

//session_start();
//require '/vagrant/public/blog-project/app/libs/Smarty.class.php';
/*  $mysqli = new mysqli('localhost','blog_user','blog_password','blog_db');
if($mysqli->connect_error){
	die('Connect_Error ('.$mysqli->maxdb_connect_errno .')'.$mysqli->connect_error);
	}else{
	echo 'DB接続しました。';
}
*/
$smarty = new Smarty;
//print_r($_GET);
//print_r($_POST);
//exit(1);

/********
getで受け取る
********/

$id = filter_input(INPUT_GET,'post_id');
if(!$id){
	$id = filter_input(INPUT_POST,'id');
}


/*********************
バリデーションの実施
入力値のチェック
*********************/
  $errors = array();//エラーの初期化
  $input_title = filter_input(INPUT_POST,'title');
  $input_body = filter_input(INPUT_POST,'body');
  $input_id = $id;

  //postされた場合
if($_SERVER["REQUEST_METHOD"] == "POST"){

	//タイトル
	if(!isset($input_title)||strlen($input_title)<=0){
	$errors['title'] = 'タイトルが入力されていません。';
	}
	elseif(strlen($input_title)>=100){
			$errors['title'] = 'タイトルは100文字以内で入力してください';
		}

	//本文
	if(!isset($input_body)||strlen($input_body)<=0){
		$errors['body'] = '本文が入力されていません';
	}
}
/***************************
編集用データの表示を行います。
****************************/
if($id){
	//プリペアステートメント
	$sql = "SELECT id, title, body FROM posts WHERE id=?";
	if($stmt = $mysqli->prepare($sql)){
		//条件値をSQLにバインドする（補足参照）
		$stmt->bind_param("i",$id);
		//実行する
		$stmt->execute();
		$stmt->bind_result($user_id,$title,$body);
		while($stmt->fetch()){
			$s_id = $user_id;
			$s_title = $title;
			$s_body = $body;
		}
		if($s_id){
			//$row = $result->fetch_assoc();
			echo "title::".$s_title;
		if($s_id){
			$smarty->assign("title",$s_title);
			$smarty->assign("body",$s_body);
			$smarty->assign("id",$s_id);
		}
		else{
			header("Location: /404.php");
			exit(1);
		}
		}
		$stmt->close();
	}
}
else{
// 	echo "GET::::";
// print_r($_GET);
// echo "<br />";
// echo "POST:::::";
// print_r($_POST);
	header("Location: /1404.php");
	exit(1);
}

/********************************
*update処理
********************************/
// echo "GET::::";
// print_r($_GET);
// echo "<br />";
// echo "POST:::::";
// print_r($_POST);

if($input_title && $input_body && !isset($errors['title']) && !isset($errors['body'])){
	//ここでSQL文を作成します。（パラメータはありません）
	$stmt = $mysqli->prepare("UPDATE posts SET title=?, body=? WHERE id=?");
	//ここでパラメーターに実際の値となる変数を入れます。
	//sssdのところは、それぞれのパラメーターの型（string,string,string,double)を指定しています。
	$rc = $stmt->bind_param('ssi',$input_title,$input_body,$id);

	if(false===$rc){
		//again execute() is useless if you can't bind the parameters. Bail out somehow.
	die('bind_param()failed:'.htmlspecialchars($stmt->error));
	}
	//プリペードステートメントを実行します。
	$rc = $stmt->execute();
	if(false===$rc){
		die('execute() failed:'.htmlspecialchars($stmt->error));
	}
	//$stmt->affected_rowsでクエリ結果を取得しています。これはinsert文などで変更された行の数を返します。
	//printf("%f Row inserted.¥n",$stmt->affected_rows);
	/*ステートメントと接続を閉じます。*/
	$stmt->close();
	//INDEXへリダイレクト
	header("Location: /blog-project/public/admin/blog/?flash_msg=blog edited.");
	exit(1);
}

$smarty->assign("errors",$errors);
$smarty->assign("blog_login_name", $_SESSION['blog_login']['name']);
//入力内容の保持
//$smarty->assign("title",$input_title);
//$smarty->assign("body",$input_body);
$smarty->assign("input_id",$id);
$smarty->display('admin/blog/edit.html');
?>