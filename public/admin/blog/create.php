<?php
require
str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'public/blog-project/app/config/config.php';
//session_start();
//require '/vagrant/public/blog-project/app/libs/Smarty.class.php';
/*  $mysqli = new mysqli('localhost','blog_user','blog_password','blog_db');
if($mysqli->connect_error){
	die('Connect_Error ('.$mysqli->maxdb_connect_errno .')'.$mysqli->connect_error);
	}else{
	echo 'DB接続しました。';
}
*/
//POSTでアクセスする
if($_SERVER["REQUEST_METHOD"] == "POST"){
  //POST値の受け取り

  $input_title = filter_input(INPUT_POST,'title',FILTER_SANITIZE_SPECIAL_CHARS);
  $input_body = filter_input(INPUT_POST,'body',FILTER_SANITIZE_SPECIAL_CHARS);
  unset($errors);

  //title
if (!isset($input_title)){
       $errors['title'] = 'タイトルが入力されていません';
   }elseif(strlen($input_title)>=100){
       $errors['title'] = 'タイトルは100文字以内で入力してください。';
   }


  //body
if (!isset($input_body)){
       $errors['body'] = '本文が入力されていません';
   }


/********************************
*登録処理
********************************/

if($input_title && $input_body && !isset($errors['title']) && !isset($errors['body'])){
	//ここでSQL文を作成する（パラメーターはありません）
	$stmt = $mysqli->prepare("INSERT INTO posts(user_id,title,body,created) VALUES(?,?,?,?)");
	//ここでパラメーターに実際の値となる変数を入れます。
	//sssdのところは、それぞれパラメータの型です。
	"INSERT INTO posts (user_id,title,body,created) VALUES ($input_userid,$input_title,$input_body,$input_created)";
	$rc = $stmt->bind_param('isss',$_SESSION['blog_login']['id'],$input_title,$input_body, date('Y-m-d H:i:s'));

	if(false===$rc){
		//again execute() is useless if you can't bind parapaters. Bailout somehow.
		die('bind_param() failed: '.htmlspecialchars($stmt->error));
			}
		/*プリペアドステートメントを実行します。*/
		$rc = $stmt->execute();
		if(false===$rc){
			die('execute()failed:'.htmlspecialchars($stmt->error));
		}
		//$stmt->affected_rowsでくえり結果を取得しています。これはインサート文で変更された行の数をかえｓもです。
		//printf("%d Row inserted.¥n",$stmt->affected_rows);
		$stmt->close();
		header("Location: /blog-project/public/admin/blog/?flash_msg=blog created.");
		exit(1);
	}

}
$smarty = new Smarty;
//テンプレートに変数を渡す
$smarty->assign("blog_login_name", $_SESSION['blog_login']['name']);
$smarty->assign("errors",$errors);
//入力内容の保持
$smarty->assign("title",$input_title);
$smarty->assign("body",$input_body);
$smarty->display('admin/blog/create.html');
?>