<?php
require
str_replace('public','',$_SERVER['DOCUMENT_ROOT']).'public/blog-project/app/config/config.php';
//session_start();
//include_once "./csrf.php";
//require '/vagrant/public/blog-project/app/libs/Smarty.class.php';
/*
  $mysqli = new mysqli('localhost','blog_user','blog_password','blog_db');
if($mysqli->connect_error){
	die('Connect_Error ('.$mysqli->maxdb_connect_errno .')'.$mysqli->connect_error);
	}else{
	echo 'DB接続しました。';
}
*/
//POSTでアクセスする
if($_SERVER["REQUEST_METHOD"] == "POST"){
  //POST値の受け取り

  $input_email = filter_input(INPUT_POST,'email',FILTER_SANITIZE_SPECIAL_CHARS);
  $input_password = filter_input(INPUT_POST,'password',FILTER_SANITIZE_SPECIAL_CHARS);
  unset($errors);

   //Eメールアドレス
if (!isset($input_email)){
	$errors['email'] ='Eメールアドレスが入力されていません';
}
	elseif(strlen($input_email)>=100){
		$errors['email'] ='Eメールアドレスは100文字以内で入力してください';
		}
	else{
		if(filter_var($input_email, FILTER_VALIDATE_EMAIL)!== false){
		//echo '正しいE-メールアドレスの形式です';
		}
	else{
		$errors['email'] ='Eメールアドレスの形式が不正です';
	}
}

  //password
if (!isset($input_password)){
       $errors['password'] = 'パスワードが送信されていません';
   }elseif(strlen($input_password)>15 || strlen($input_password)<=6) {
       $errors['password'] = '6文字以上15文字未満で入力してください。';
   }else{
   	if (preg_match("/^[a-zA-Z0-9]+$/", $input_password)){
   		//echo '正しいパスワード形式です';
   }
   else{
       $errors['password'] = 'パスワードの形式が不正です';
   }
}
     //tokenチェック
    //CsrfValidator::checkToken();
    /*if (!CsrfValidator::validate(filter_input(INPUT_POST, 'token'))) {
        header('Content-Type: text/plain; charset=UTF-8', true, 400);
        die('CSRF validation failed.');
    }
    */
       //ログインIDを保持するがチェックされた場合、CookieにIDを保存する
   $login_id = filter_input(INPUT_POST, 'login_id', FILTER_SANITIZE_SPECIAL_CHARS);
 if($login_id){
    setcookie('sachool_login_id',$login_id,time()+3600);//60分間の有効期限
  }

  //入力チェックが終わり、errorが無ければ
  //DBを読み込んでIDとパスワードの突き合わせをする。

if($input_email && $input_password && !isset($errors['$email']) && !isset($errors['password'])){
	//SELECT文の雛形をもとにステートメントハンドルを取得する
	//プリペアステートメント
	$sql = "SELECT id FROM users WHERE email=? AND password=?";
	if($stmt = $mysqli->prepare($sql)){
		//条件値をSQLにバインドする（補足参照）
		$stmt->bind_param("ss",$input_email, $input_password);
		//実行
		$stmt->execute();
		//取得結果を変数にバインドする
		$stmt->bind_result($id);
		//ログイン成功
		while($stmt->fetch()){
			if($id){
			$_SESSION['blog_login']['id'] = $id;
			$_SESSION['blog_login']['name'] = $name;
			  header("Location: ./admin/mypage.php");
			  exit(1);
			}
		}
		//ログイン失敗
		$errors['login'] ='メールアドレスまたはパスワードが違います';
		echo $_SESSION['login_id'];

		$stmt->close();
	}
}
}
$smarty = new Smarty;
$smarty->assign("errors",$errors);
$smarty->display('login.html');
?>