<?php
/*
*環境変数用
*env.php
*/

define('PROJECT_DIR','/vagrant/public/blog-project/');
define('APP_DIR',PROJECT_DIR.'app/'); // /vagrant/public/blog-project/app/
define('LIBS_DIR',APP_DIR.'libs/');	  // /vagrant/public/blog-project/app/libs
define('TEMPLATE_DIR',APP_DIR.'templates/'); // /vagrant/public/blog-project/app/templates/
define('CONFIG_DIR',APP_DIR.'config/');	//vagrant/blog-project/app/config
define('PUB_DIR',PROJECT_DIR.'public/'); //今回のプロジェクトのドキュメントルート

/*
*サイト設定
*/

define('_SITE_TITLE','観光どっとこむ'); //ブログのタイトル
define('_SITE_PHRASE','観光の促進を目的'); //ブログのキャッチフレーズ
define('_SITE_BLOG_PER_NUM','5'); //１ページに表示させる件数
define('_SITE_BLOG_BODY_ALL','0'); //一覧に全文を表示させる:1、表示させない:0
define('_SITE_BLOG_DESIGN','1'); //simple;1、main-visual:2
?>