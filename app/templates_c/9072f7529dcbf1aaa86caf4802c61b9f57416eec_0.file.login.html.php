<?php
/* Smarty version 3.1.31, created on 2017-11-03 20:42:39
  from "/vagrant/public/blog-project/app/templates/login.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59fc562f21aa32_17997678',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9072f7529dcbf1aaa86caf4802c61b9f57416eec' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/login.html',
      1 => 1509706185,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59fc562f21aa32_17997678 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Signin Template for Bootstrap</title>

<!--Bootstrap core CSS-->
<link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this templates-->
<link href="/blog-project/public/css/login.css" rel="stylesheet">
</head>
<body>
	<div class="container">
     <form class="form-signin" action="login.php" method="post">

       <h2 class="form-signin-heading">Please Log in</h2>

       <?php if ($_smarty_tpl->tpl_vars['errors']->value) {?>
       <div class="alert alert-error bg-danger has-error">
         <span class="help-block">メールアドレスまたはパスワードが違います。</span>
       </div>
       <?php }?>
       <?php if ($_smarty_tpl->tpl_vars['flash_msg']->value) {?>
       <div class="alert alert-error bg-danger has-error">
          <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['flash_msg']->value;?>
</span>
        </div>
       <?php }?>

       <div class="form-group control-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['email']) {?> has-error error<?php }?>">
         <label for="inputEmail" class="sr-only">Email address</label>
         <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
         <?php if ($_smarty_tpl->tpl_vars['errors']->value['email']) {?> <span class="help-block"> <?php echo $_smarty_tpl->tpl_vars['errors']->value['email'];?>
</span><?php }?>
       </div>

       <div class="form-group control-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['password']) {?>has-error error<?php }?>">
         <label for="inputPassword" class="sr-only">Password</label>
         <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
         <?php if ($_smarty_tpl->tpl_vars['errors']->value['password']) {?> <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['password'];?>
</span><?php }?>
        </div>

       <div class="checkbox">
         <label>
           <input type="checkbox" value="remember-me"> Remember me
         </label>
       </div>
       <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
     </form>
   </div>
</body>
</html><?php }
}
