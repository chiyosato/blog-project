<?php
/* Smarty version 3.1.31, created on 2017-11-02 19:19:19
  from "/vagrant/public/blog-project/app/templates/admin/blog/edit.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59faf12731b864_73547180',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '27dff2bf7bc7c69dffe2e4669f596d471c4fabac' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/blog/edit.html',
      1 => 1509618084,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59faf12731b864_73547180 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">
  <title>ブログ編集画面</title>

</head>
		<body>
    <!-- header-->
 <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--header End-->
    <!-- sidebar-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--sidebar -->
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
       	<h1 class="form-signin-heading">ブログの編集</h1>
        <div>
        <?php if ($_smarty_tpl->tpl_vars['errors']->value) {?>
        <div class="alert alert-error bg-danger has-error">
          <span class="help-block">入力にエラーがあります。</span>
        </div>
        <?php }?>
        	<form role="form" action="./edit.php" method="post">
         <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['title']) {?>has-error error<?php }?>">
         <label for="usr">タイトル</label>
         <input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['title']->value;?>
" class="form-control"
         id="user">
         <?php if ($_smarty_tpl->tpl_vars['errors']->value['title']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['title'];?>
</span>
         <?php }?>
          </div>

         <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['body']) {?>has-error error<?php }?>">
           <label for="pwd" >本文:</label>
           <textarea id="aaa" name="body" class="form-control" ><?php echo $_smarty_tpl->tpl_vars['body']->value;?>
 </textarea>
            <?php if ($_smarty_tpl->tpl_vars['errors']->value['body']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['body'];?>
</span>
        <?php }?>
         </div>

         <button type="submit" class="btn btn-lg btn-primary">登録</button>
         <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
         <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
">
       </form>
        </div>
      </div>
  <!--footer-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!-- Bootstrap core CSS -->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/blog.css" rel="stylesheet">
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">

    <?php echo '<script'; ?>
 src="/blog-project/public/js/tinymce.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
    tinymce.init({
        selector: "#aaa", // id="foo"の場所にTinyMCEを適用
    });
    <?php echo '</script'; ?>
>
	<!--footer-->
  	</body>
</html><?php }
}
