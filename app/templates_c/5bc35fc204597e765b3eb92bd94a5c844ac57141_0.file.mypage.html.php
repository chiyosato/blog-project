<?php
/* Smarty version 3.1.31, created on 2017-10-31 12:29:28
  from "/vagrant/public/blog-project/app/templates/admin/mypage.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59f7ee1863fdc2_40082155',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5bc35fc204597e765b3eb92bd94a5c844ac57141' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/mypage.html',
      1 => 1509420557,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f7ee1863fdc2_40082155 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Welcome to my page</title>

    <!-- Bootstrap core CSS -->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
      <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
  </head>

  <body>

    <!-- header-->
 <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--header End-->
    <!--
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Top</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.html">アカウント設定</a></li>
            <li><a href="/blog-project/public/logout.php">ログアウト</a></li>
          </ul>
        </div>
      </div>
    </nav>
  -->
  <!-- sidebar-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <!--sidebar -->
    <!--
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="index.html">マイページTOP</a></li>
            <li><a href="/blog-project/public/admin/blog/index.php">ブログ一覧</a></li>
          </ul>
        </div>
    -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">My page</h1>

          <h2 class="sub-header">ようこそ</h2>
         <!--

                  <h3>アカウント設定を更新してください</h3>
      <div class="container">
      <form action="index.php" method="post">

        <div><p><label>名前</label><input type="text" name="name" </p></div>
        <div><p><label>メール</label><input type="text" name="email" </p></div>
        <div><p><label>パスワード</label><input type="text" name="password" ></p></div>
        <div><p><label>年齢</label><input type="int" name="age" </p></div>
        <input type="submit" value="送信する"><br /><br /><br />
        <input type="hidden" name="update" value="up-account">
      </form>
      </div>
          -->
  <!--footer-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <!--footer End-->

  </body>
</html>
<?php }
}
