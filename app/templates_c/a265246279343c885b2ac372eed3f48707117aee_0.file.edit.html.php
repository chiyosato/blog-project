<?php
/* Smarty version 3.1.31, created on 2017-11-04 16:29:24
  from "/vagrant/public/blog-project/app/templates/admin/setting/edit.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59fd6c541abe36_89158446',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a265246279343c885b2ac372eed3f48707117aee' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/setting/edit.html',
      1 => 1509780557,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59fd6c541abe36_89158446 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_html_radios')) require_once '/vagrant/public/blog-project/app/libs/plugins/function.html_radios.php';
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

    <title>ブログの編集設定</title>
    <!--Bootstrap core CSS-->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">
    <link href="/blog-project/public/css/blog.css" rel="stylesheet">
  </head>
	<body>
  <!-- header -->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <!-- header End -->

    <div class="container-fluid">
      <div class="row">

        <!-- sidebar -->
        <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        <!-- sidebar End -->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h3 class="page-header">ブログの編集設定</h3>
          <div>
     		<?php if ($_smarty_tpl->tpl_vars['errors']->value) {?>
            <div class="alert alert-error bg-danger has-error">
              <span class="help-block">入力にエラーがあります。</span>
            </div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['flash_msg']->value) {?>
            <div class="alert alert-success" role="alert">
              <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['flash_msg']->value;?>
</span>
            </div>
            <?php }?>
            <form role="form" action="./edit.php" method="post">

      		<div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section__SITE_TITLE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section__SITE_TITLE']->value['index'] : null)]) {?>has-error error<?php }?>">
                <label for="usr">ブログのタイトル:</label>
                <input type="text" name="_SITE_TITLE" value="<?php echo $_smarty_tpl->tpl_vars['_SITE_TITLE']->value;?>
" class="form-control" id="usr">
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_TITLE']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['_SITE_TITLE'];?>
</span><?php }?>
            </div>

    		 <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_PHRASE']) {?>has-error error<?php }?>">
                <label for="usr">ブログキャッチフレーズ:</label>
                <input type="text" name="_SITE_PHRASE" value="<?php echo $_smarty_tpl->tpl_vars['_SITE_PHRASE']->value;?>
" class="form-control" id="usr">
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_PHRASE']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['_SITE_PHRASE'];?>
</span><?php }?>
             </div>

			<div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_PER_NUM']) {?>has-error error<?php }?>">
                <label for="usr">1ページに表示させる件数:</label>
                <div style="margin-left:2em;"><?php echo smarty_function_html_radios(array('name'=>'_SITE_BLOG_PER_NUM','options'=>$_smarty_tpl->tpl_vars['blog_show_num']->value,'selected'=>$_smarty_tpl->tpl_vars['_SITE_BLOG_PER_NUM']->value,'separator'=>'<br />'),$_smarty_tpl);?>
</div>
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_PER_NUM']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_PER_NUM'];?>

                </span><?php }?>
            </div>
            <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_BODY_ALL']) {?>has-error error<?php }?>">
                <label for="usr">一覧に全文を表示させる:</label>
                <div style="margin-left:2em;"><?php echo smarty_function_html_radios(array('name'=>'_SITE_BLOG_BODY_ALL','options'=>$_smarty_tpl->tpl_vars['show_radios']->value,'selected'=>$_smarty_tpl->tpl_vars['_SITE_BLOG_BODY_ALL']->value,'separator'=>'<br />'),$_smarty_tpl);?>
</div>
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_BODY_ALL']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_BODY_ALL'];?>
</span><?php }?>
              </div>
              <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_DESIGN']) {?>has-error error<?php }?>">
                <label for="usr">ブログのデザイン:</label>
                <div style="margin-left:2em;"><?php echo smarty_function_html_radios(array('name'=>'_SITE_BLOG_DESIGN','options'=>$_smarty_tpl->tpl_vars['blog_design']->value,'selected'=>$_smarty_tpl->tpl_vars['_SITE_BLOG_DESIGN']->value,'separator'=>'<br />'),$_smarty_tpl);?>
</div>
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_DESIGN']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['_SITE_BLOG_DESIGN'];?>
</span><?php }?>
              </div>

              <button type="submit" class="btn btn-lg btn-primary">更新</button>
              <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
              <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
">
		</form>
          </div>

        </div>
      </div>
    </div>
	<!-- footer -->
    <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!-- footer End -->
	</body>
</html><?php }
}
