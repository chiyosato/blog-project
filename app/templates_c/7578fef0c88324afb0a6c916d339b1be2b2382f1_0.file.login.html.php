<?php
/* Smarty version 3.1.31, created on 2017-10-19 16:36:46
  from "/vagrant/public/blog-project/public/login.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59e8560ee76d39_55404920',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7578fef0c88324afb0a6c916d339b1be2b2382f1' => 
    array (
      0 => '/vagrant/public/blog-project/public/login.html',
      1 => 1508398733,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e8560ee76d39_55404920 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>Signin Template for Bootstrap</title>

<!--Bootstrap core CSS-->
<link href="/css/bootstrap.min.css" rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows8 bug -->
<link href="../../assets/css/ie10-viewport-bug-workaroud.css" rel="stylesheet">

<!-- Custom styles for this templates-->
<link href="/css/login.css" rel="stylesheet">
<!-- just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE9]><?php echo '<script'; ?>
 src="../../assets/js/ie8-responsive-file-warnig.js"><?php echo '</script'; ?>
><![endif]-->
	<?php echo '<script'; ?>
 src="../../assets/js/ie-emulation-modes-warning.js"><?php echo '</script'; ?>
>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE9]>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
			<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
		<![endif]-->
</head>
<body>
	<div class="container">
     <form class="form-signin" action="/login.php" method="post">
       <h2 class="form-signin-heading">Please Log in</h2>
       <div class="alert alert-error bg-danger has-error">
         <span class="help-block">メールアドレスまたはパスワードが違います。</span>
       </div>
       <div class="form-group control-group has-error error">
         <label for="inputEmail" class="sr-only">Email address</label>
         <input type="email" name="email" id="inputEmail inputError" class="form-control" placeholder="Email address" required autofocus>
         <span class="help-block">入力が不正です。</span>
       </div>

       <div class="form-group control-group has-error error">
         <label for="inputPassword" class="sr-only">Password</label>
         <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
         <span class="help-block">入力が不正です。</span>
       </div>

       <div class="checkbox">
         <label>
           <input type="checkbox" value="remember-me"> Remember me
         </label>
       </div>
       <button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
     </form>
   </div> <!-- /container -->
</body>
</html><?php }
}
