<?php
/* Smarty version 3.1.31, created on 2017-11-03 15:56:10
  from "/vagrant/public/blog-project/app/templates/admin/blog/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59fc130af40639_99436786',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e01bc3ac0715f4d751cef69672fccf1287a5a0a1' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/blog/index.html',
      1 => 1509638969,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59fc130af40639_99436786 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/vagrant/public/blog-project/app/libs/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_replace')) require_once '/vagrant/public/blog-project/app/libs/plugins/modifier.replace.php';
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/blog.css" rel="stylesheet">
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">

  </head>

  <body>
    <!-- header-->
 <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--header End-->

    <div class="container-fluid">
      <div class="row">
          <!-- sidebar-->
      <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

           <!--sidebar -->
       <!--
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
            <li><a href="#">ブログ一覧</a></li>
          </ul>
        </div>
      -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">BLOG</h1>
          <button>
          <a href="/blog-project/public/admin/blog/create.php">新規投稿</a>
          </button>
          <h3 class="sub-header">ブログ一覧</h3>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>タイトル</th>
                  <th>日時</th>
                  <th>本文</th>
                  <th>編集</th>
                </tr>
              </thead>
              <tbody>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blogs']->value, 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
                <tr>
                  <td><?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
</td>
                  <td><?php echo $_smarty_tpl->tpl_vars['value']->value['created'];?>
</td>
                  <td><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['value']->value['body'],10);?>
</td>
                  <td>
                  <button type="button" class="btn btn-success" onClick="location.href='./edit.php?post_id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
'">編集</button>
                  <a href ="./delete.php?post_id=<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
&common=true" class="confirm" id="alert" title="ブログ【<?php echo $_smarty_tpl->tpl_vars['value']->value['title'];?>
】を削除します。<br />よろしいですか？"><button type="button" class="btn btn-danger">削除</button></a>
                  </td>
                </tr>
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

              </tbody>
            </table>
                <nav>
                <ul class="pagination"><?php echo smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['links']->value['all'],'<a ','<li><a '),'</a>','</a></li>'),'<span>',''),'</span>','');?>
</ul>
                </nav>
          </div>
        </div>
      </div>
    </div>


  <!--footer-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <!--footer End-->

  </body>
</html><?php }
}
