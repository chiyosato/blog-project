<?php
/* Smarty version 3.1.31, created on 2017-10-24 23:45:37
  from "/vagrant/public/blog-project/app/templates/admin/blog/update.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59ef521181cb20_81020179',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '787154f1aa0fbe9c8ecee25dfa73e6ee04f6b12c' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/blog/update.html',
      1 => 1508776099,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59ef521181cb20_81020179 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">
  <title>ブログ編集画面</title>

<!--Bootstrap core CSS-->
<link href="/blog-project/public/css/blog.css" rel="stylesheet">
<!-- Custom styles for this templates
<link href="/blog-project/public/css/login.css" rel="stylesheet">
-->
<!-- just for debugging purposes. Don't actually copy these 2 lines! -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE9]>
		<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
			<?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
		<![endif]-->
<head>
    <meta charset="UTF-8">
    <?php echo '<script'; ?>
 src="./tinymce/js/tinymce/tinymce.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
    tinymce.init({
        selector: "#aaa", // id="foo"の場所にTinyMCEを適用
    });
    <?php echo '</script'; ?>
>

</head>
		<body>
			<div class="container">
     		<form class="form-signin" action="update.php" method="post">
			<h2 class="form-signin-heading">ブログを書く</h2>

       <div class="alert alert-error bg-danger has-error">
         <span class="help-block"></span>
       </div>
       <div class="alert alert-error bg-danger has-error">
          <span class="help-block"></span>
        </div>

       <div class="form-group control-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['title']) {?> has-error error<?php }?>">
         <label for="title" class="sr-only">タイトル</label>
         <input type="title" name="title" id="title" class="text" placeholder="title">
         <span class="help-block"></span>
       </div>

      <textarea id="aaa" name="body"></textarea>

       </div>
       <button class="btn btn-lg btn-primary btn-block" type="submit">更新する</button>
     </form>


		</body>
</html><?php }
}
