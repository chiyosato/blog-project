<?php
/* Smarty version 3.1.31, created on 2017-11-09 23:27:04
  from "/vagrant/public/blog-project/app/templates/index.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a0465b8d3c0f3_56626248',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '230713449a714f0f5284e08f97565cf921add395' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/index.html',
      1 => 1510237177,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a0465b8d3c0f3_56626248 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_truncate')) require_once '/vagrant/public/blog-project/app/libs/plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_replace')) require_once '/vagrant/public/blog-project/app/libs/plugins/modifier.replace.php';
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo @constant('_SITE_TITLE');?>
</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  </head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
         <!--    <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Start Bootstrap</a>
            </div>
 -->
            <!-- Collect the nav links, forms, and other content for toggling -->
        <!--     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="post.html">Sample Post</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                </ul>
            </div> -->
        </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" >
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto">
            <div class="site-heading">
              <h1><?php echo $_smarty_tpl->tpl_vars['_SITE_TITLE']->value;?>
</h1>
              <span class="subheading"><?php echo @constant('_SITE_PHRASE');?>
</span>
            </div>
          </div>
        </div>
      </div>
    </header>


    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blogs']->value, 'v', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['v']->value) {
?>
          <div class="blog-post">
            <h2 class="blog-post-title"><a href="page.php?post_id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['title'];?>
</a></h2>
            <p class="blog-post-meta"><?php echo $_smarty_tpl->tpl_vars['v']->value['created'];?>
 by <a href="mailto:test@sachool.jp">satoshi</a></p>

            <?php if ($_smarty_tpl->tpl_vars['_SITE_BLOG_BODY_ALL']->value) {?>
              <?php echo $_smarty_tpl->tpl_vars['v']->value['body'];?>

            <?php } else { ?>
              <?php echo smarty_modifier_truncate(strip_tags($_smarty_tpl->tpl_vars['v']->value['body']),300,"...",true);?>

            <?php }?>

          </div><!-- /.blog-post -->
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                <!-- Pager -->
                <nav>
                  <ul class="pager">
                    <?php echo smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['links']->value['back'],'<a ','<li><a '),'</a>','</a></li>'),'<span>',''),'</span>','');?>

                    <?php echo smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['links']->value['next'],'<a ','<li><a '),'</a>','</a></li>'),'<span>',''),'</span>','');?>

                  </ul>
                </nav>

            </div>
        </div>
    </div>

    <hr>

    <!-- jQuery -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"><?php echo '</script'; ?>
>

    <!-- Bootstrap Core JavaScript -->
    <?php echo '<script'; ?>
 src="js/bootstrap.min.js"><?php echo '</script'; ?>
>

    <!-- Custom Theme JavaScript -->
    <?php echo '<script'; ?>
 src="js/clean-blog.min.js"><?php echo '</script'; ?>
>

  </body>

</html><?php }
}
