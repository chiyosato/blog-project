<?php
/* Smarty version 3.1.31, created on 2017-11-02 19:28:00
  from "/vagrant/public/blog-project/app/templates/admin/blog/create.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59faf3308089c4_72032718',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3630950fc0cbbb90d8ae54b425d8f43c9d3892b6' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/blog/create.html',
      1 => 1509618536,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59faf3308089c4_72032718 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">
  <title>ブログ編集画面</title>
      <!-- Bootstrap core CSS -->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/blog.css" rel="stylesheet">
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">
</head>
		<body>
    <!-- header-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--header End-->
    <!-- sidebar-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!--sidebar -->
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h3 class="page-header">BLOGを書く</h3>
    <div>
         <?php if ($_smarty_tpl->tpl_vars['errors']->value) {?>
       <div class="alert alert-error bg-danger has-error">
         <span class="help-block">入力にエラーがあります。</span>
       </div>
       <?php }?>
    <form role="form" action="create.php" method="post">
       <?php if ($_smarty_tpl->tpl_vars['flash_msg']->value) {?>
       <div class="alert alert-error bg-danger has-error">
          <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['flash_msg']->value;?>
</span> <?php }?>
      </div>

       <div class="form-group control-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['title']) {?> has-error error<?php }?>">
         <label for="usr">タイトル:</label>
         <input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['title']->value;?>
" class="form-control" id="usr">
         <?php if ($_smarty_tpl->tpl_vars['errors']->value['title']) {?> <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['title'];?>
</span><?php }?>
       </div>

       <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['body']) {?>has-error error<?php }?>">
        <label for="pwd">本文:</label>
      <textarea id="aaa" name="body" class="form-control"><?php echo $_smarty_tpl->tpl_vars['body']->value;?>
</textarea>
      <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
">
       <button type="submit" class="btn btn-lg btn-primary">登録する</button>
     </form>
      </div>
	</div>
    <!--footer-->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <?php echo '<script'; ?>
 src="/blog-project/public/js/tinymce.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
    tinymce.init({
        selector: "#aaa", // id="foo"の場所にTinyMCEを適用
    });
    <?php echo '</script'; ?>
>
  <!--footer End-->
  	</body>
</html><?php }
}
