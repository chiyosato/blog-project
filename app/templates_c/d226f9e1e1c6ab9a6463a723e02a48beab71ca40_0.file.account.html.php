<?php
/* Smarty version 3.1.31, created on 2017-11-04 12:58:54
  from "/vagrant/public/blog-project/app/templates/admin/account/account.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_59fd3afe15aa63_60352578',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd226f9e1e1c6ab9a6463a723e02a48beab71ca40' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/account/account.html',
      1 => 1509766168,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59fd3afe15aa63_60352578 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, intial-scale=1">
	<!--The above 3 meta tags *must* come first in the head; any other head
content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../../favicon.ico">

	<title>アカウント設定画面</title>
	    <!-- Bootstrap core CSS -->
    <link href="/blog-project/public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/blog-project/public/css/dashboard.css" rel="stylesheet">
    <link href="/blog-project/public/css/blog.css" rel="stylesheet">
</head>
<body>
  <!-- header -->
  <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_header.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

  <!-- header End -->

    <div class="container-fluid">
      <div class="row">

        <!-- sidebar -->
        <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_sidebar.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

        <!-- sidebar End -->

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">アカウントの編集</h1>
          <div>
              <?php if ($_smarty_tpl->tpl_vars['errors']->value) {?>
            <div class="alert alert-error bg-danger has-error">
              <span class="help-block">入力にエラーがあります。</span>
            </div>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['flash_msg']->value) {?>
            <div class="alert alert-success" role="alert">
              <span class="help-block"><?php echo $_smarty_tpl->tpl_vars['flash_msg']->value;?>
</span>
            </div>
            <?php }?>

            <form role="form" action="./edit.php" method="post">

              <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['name']) {?>has-error error<?php }?>">
                <label for="usr">名前:</label><span class="label label-default">100文字以内</span>
                <input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" class="form-control" id="usr">
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['name']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['name'];?>
</span><?php }?>
              </div>
              <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['email']) {?>has-error error<?php }?>">
                <label for="usr">メールアドレス:</label><span class="label label-default">100文字以内</span>
                <input type="text" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" class="form-control" id="usr">
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['email']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['email'];?>
</span><?php }?>
              </div>
              <div class="form-group <?php if ($_smarty_tpl->tpl_vars['errors']->value['password']) {?>has-error error<?php }?>">
                <label for="usr">パスワード:</label>
                <input type="password" name="password" value="<?php echo $_smarty_tpl->tpl_vars['password']->value;?>
" class="form-control" id="usr">
                <?php if ($_smarty_tpl->tpl_vars['errors']->value['password']) {?><span class="help-block"><?php echo $_smarty_tpl->tpl_vars['errors']->value['password'];?>
</span><?php }?>
              </div>
              <button type="submit" class="btn btn-lg btn-primary">更新</button>
              <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
              <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
">
            </form>
          </div>

        </div>
      </div>
    </div>

    <!-- footer -->
    <?php $_smarty_tpl->_subTemplateRender((@constant('TEMPLATE_DIR')).('admin/layout/_footer.html'), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

    <!-- footer End -->
  </body>
</html><?php }
}
