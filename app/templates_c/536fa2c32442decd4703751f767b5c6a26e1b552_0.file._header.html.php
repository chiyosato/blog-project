<?php
/* Smarty version 3.1.31, created on 2017-11-09 18:07:37
  from "/vagrant/public/blog-project/app/templates/admin/layout/_header.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a041ad9987b66_81076479',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '536fa2c32442decd4703751f767b5c6a26e1b552' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/layout/_header.html',
      1 => 1510218451,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a041ad9987b66_81076479 (Smarty_Internal_Template $_smarty_tpl) {
?>
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/blog-project/public/admin/mypage.php">mypage</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/blog-project/public/admin/account/edit.php">アカウント編集</a></li>
            <li><a href="/blog-project/public/admin/logout.php?confirm=true" class="confirm" title="Logoutしますよろしいですか？">ログアウト</a></li>
          </ul>
          <form class="navbar-form" navar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav><?php }
}
