<?php
/* Smarty version 3.1.31, created on 2017-11-09 18:18:50
  from "/vagrant/public/blog-project/app/templates/admin/layout/_sidebar.html" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a041d7aa76819_97206962',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '31be6e7939128b0f3feaf7cf5f9c337a23cc8f3e' => 
    array (
      0 => '/vagrant/public/blog-project/app/templates/admin/layout/_sidebar.html',
      1 => 1510218520,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a041d7aa76819_97206962 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		<li <?php if ($_SERVER['SCRIPT_NAME'] == '/admin/index.php') {?>class= "active"<?php }?>><a href="/blog-project/public/admin/mypage.php">HOME</a></li>
		<li <?php if (mb_strpos($_SERVER['SCRIPT_NAME'],'blog')) {?>class="active"<?php }?>>
			<a href="/blog-project/public/admin/blog/index.php">BLOGを書く</a></li>
		<li>
			<a href="/blog-project/public/admin/account/edit.php">プロフィール編集</a>
		</li>
		<li><a href="/blog-project/public/admin/setting/edit.php">ブログの編集設定</a>
		</li>
		<!--
		<li><a href="#">Export</a></li>
		-->
	</ul>
</div><?php }
}
